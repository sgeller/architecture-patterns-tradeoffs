# Architecture Patterns Tradeoffs

| pattern | agility | deployment | testability | performance | scalability | simplicity | cost |
| -------- | :--------: |:--------: | :--------: | :--------: | :--------: | :--------: | :--------: |
| [Layered](https://en.wikipedia.org/wiki/Multitier_architecture) | ❌ | ❌ | ✔ | ❌ | ❌ | ✔ | $ |
| [Microkernel]() | ✔ | ✔ | ✔ | ❌ | ❌ | ✔ | $$ |
| [Event-Driven]() | ✔ | ✔ | ❌ | ✔ | ✔ | ❌ | $$$ |
| [Pipeline]() | ✔ | ❌ | ✔ | ❌ | ❌ | ✔ | $ |
| [Space-based](https://en.wikipedia.org/wiki/Space-based_architecture) | ✔ | ✔ | ❌ | ✔ | ✔ | ❌ | $$$$ |
| [Microservices]() | ✔ | ✔ | ✔ | ❌ | ✔ | ❌ | $$$ |
| [Service-Oriented]() | ❌ | ❌ | ❌ | ❌ | ✔ | ❌ | $$$$ |
| [Service-Based]() | ✔ | ✔ | ✔ | ❌ | ✔ | ❌ | $$ |
| Serverless |


[Source](https://learning.oreilly.com/videos/software-architecture-fundamentals/9781491998991/9781491998991-video316994)

